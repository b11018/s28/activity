/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.
*/			
db.users.insertMany([
	{
		firstName : "Michael",
		lastName : "League",
		email: "mleague@mail.com",
		password: "snarkyBass",
		isAdmin: false
	},
	{
		firstName : "Mark",
		lastName : "Lettieri",
		email: "mlettieri@mail.com",
		password: "snarkyGuitar",
		isAdmin: false
	},
	{
		firstName : "Shaun",
		lastName : "Martin",
		email: "smartin@mail.com",
		password: "snarkyKeys",
		isAdmin: false
	},
	{
		firstName : "Larnell",
		lastName : "Lewis",
		email: "llewis@mail.com",
		password: "snarkyDrums",
		isAdmin: false
	},
	{
		firstName : "Bob",
		lastName : "Reynolds",
		email: "breynolds@mail.com",
		password: "snarkySax",
		isAdmin: false
	}
])
/*
	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 
*/
db.courses.insertMany([
	{
		name : "Fundamentals of Music",
		price : 150,
		isActive: false
	},
	{
		name : "Basic Rhythm Section",
		price : 275,
		isActive: false
	},
	{
		name : "Advanced Orchestration",
		price : 325,
		isActive: false
	}
])
/*
//Read

	>> Find all regular/non-admin users.
*/
db.users.find({isAdmin: false})
/*
//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.
*/
db.users.updateOne({},
	{
		$set:
		{
			isAdmin : true
		}
	}
)
db.courses.updateOne(
	{
		name: "Fundamentals of Music"
	},
	{
		$set:
		{
			isActive : true
		}
	}
)
/*
//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js

*/
db.courses.deleteMany({isActive: false})